<?php

class Models_Mapper_Fbgraph extends Application_Model_Mappers_Abstract{
	
	protected $_dbTable = 'Models_DbTable_Fbgraph';

	protected $_model   = 'Models_Model_Fbgraph';
	
	public function save($_model){}
	
    public function getConf(){
		if($this->getDbTable()->fetchAll()->current())
		{
			return $this->getDbTable()->fetchAll()->current()->toArray();
		} 
		else 
		{
			return false;
		};
    }
    public function setConf($id, $secret, $userId, $language, $uri){
            $data = array(
                "app_id"		  => $id,
                "app_secret"      => $secret,
				"user_id"         => $userId,
                "fbLanguage"      => $language,
                "redirectUri"     => $uri
            );
		 return $this->getDbTable()->update($data, true);
	}
	public function setToken($token){
		   $data = array(
				"token"        => $token
            );
		 return $this->getDbTable()->update($data, true);
	}
	
}