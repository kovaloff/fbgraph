<?php class Models_Model_Fbgraph extends Application_Model_Models_Abstract {
	
		protected $_appId = '';

		protected $_appSecret = '';

		protected $_userId = '';
		
		protected $_token = '';

        protected $_fbLanguage = '';

        protected $_redirectUri = '';
	
	

	public function getAppId(){
		return $this->_appId;
	}

	public function setAppId($appId){
		$this->_appId = $appId;
		return $this;
	}

	public function getAppSecret(){
		return $this->_appSecret;
	}

	public function setAppSecret($appSecret){
		$this->_appSecret = $appSecret;
		return $this;
	}

	public function getUserId(){
		return $this->_userId;
	}

	public function setUserId($userId){
		$this->_userId = $userId;
		return $this;
	}
	public function getToken(){
		return $this->_token;
	}

	public function setToken($token){
		$this->_token = $token;
		return $this;
	}
    public function getFbLanguage(){
        return $this->_fbLanguage;
    }

    public function setFbLanguage($fbLanguage){
        $this->_fbLanguage = $fbLanguage;
        return $this;
    }

    public function getRedirectUri(){
        return $this->_redirectUri;
    }

    public function setRedirectUri($redirectUri){
        $this->_redirectUri = $redirectUri;
        return $this;
    }

}
   
