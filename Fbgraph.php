<?php
include_once 'system/app/src/facebook.php';
class Fbgraph extends Tools_Plugins_Abstract{
	public function  __construct($options, $seotoasterData) {
		parent::__construct($options, $seotoasterData);
		$this->_seotoasterData = $seotoasterData;
		$this->_view->setScriptPath(dirname(__FILE__) . '/system/views/');
		$this->_sessionHelper->fbauthData = Models_Mapper_Fbgraph::getInstance()->getConf();
		$config = array(
				'appId' => $this->_sessionHelper->fbauthData['app_id'],
				'secret' => $this->_sessionHelper->fbauthData['app_secret'],
				'cookie' => true
			  );
	    $this->_facebook = new Facebookfbgraph($config);
	}
    public function run($requestedParams = array()) {
		$dispatchersResult      = parent::run($requestedParams);
		$this->_view->token       = $this->_sessionHelper->fbauthData['token'];
		$this->_view->appId		  = $this->_sessionHelper->fbauthData['app_id'];
		$this->_view->callbackUrl = $this->_sessionHelper->fbauthData['redirectUri'];
        /**
         * Fbmeta Part start
         */
        if($this->_options[0] == 'share') {
            return $this->_view->render("fbmeta.phtml");
        }
        if($this->_options[0] == 'ogurl') {
            return $this->_seotoasterData['websiteUrl'].$this->_seotoasterData['url'];
        }
        if($this->_options[0] == 'tags') {
            $this->_view->AdminLogged = Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PLUGINS);
            $pageId = $this->_seotoasterData['id'];
            $fbTitle = $this->_seotoasterData['h1'];
            $fbUrl = $this->_seotoasterData['websiteUrl'].$this->_seotoasterData['url'];
            $fbTeaserImg = Tools_Page_Tools::getPreviewPath($pageId);
            $fbSiteName = $_SERVER['SERVER_NAME'];
            $fbType = 'website';
            preg_match("/\.[-a-zA-Z_]*\./", $fbSiteName, $name);
            if(!empty($name)){
                $fbSiteName = substr($name['0'], 1, -1);
            }
            $fbDescription = $this->_seotoasterData['teaserText'];
            $fbMeta  = '<meta property="og:title" content="'.$fbTitle.'"/>'."\n";
            $fbMeta .= '<meta property="og:type" content="'.$fbType.'"/>'."\n";
            $fbMeta .= '<meta property="og:url" content="'.$fbUrl.'"/>'."\n";
            $fbMeta .= '<meta property="og:image" content="'.$fbTeaserImg.'"/>'."\n";
            $fbMeta .= '<meta property="og:site_name" content="'.$fbSiteName.'"/>'."\n";
            $fbMeta .= '<meta property="og:description" content="'.$fbDescription.'"/>'."\n";
            return $fbMeta;

        }
        /**
         * Fbmeta part end
         */
        /**
         * Fbcomments part start
         */
        if(isset($this->_options[0]) && ($this->_options[0] == 'ogtag')){
            $this->_view->AdminLogged = Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PLUGINS);
            $this->_view->pageUrl = $this->_seotoasterData['websiteUrl'].$this->_seotoasterData['url'];
            $this->_view->fbLanguage = $this->_sessionHelper->fbauthData['fbLanguage'];
            $fbTitle = $this->_seotoasterData['h1'];
            $fbUrl = $this->_seotoasterData['websiteUrl'].$this->_seotoasterData['url'];
            $fbTeaserImg = Tools_Page_Tools::getPreviewPath($this->_seotoasterData['id']);
            $fbAdmin = $this->_sessionHelper->fbauthData['app_id'];
            $ogTag   = '<meta property="fb:admins" content="'.$fbAdmin.'"/>'."\n";
            $ogTag  .= '<meta property="og:url" content="'.$fbUrl.'"/>'."\n";
            $ogTag  .= '<meta property="og:title" content="'.$fbTitle.'"/>'."\n";
            $ogTag  .= '<meta property="og:image" content="'.$fbTeaserImg.'"/>'."\n";
            $ogTag  .= '<meta property="og:locale" content="'.$this->_sessionHelper->fbauthData['fbLanguage'].'"/>'."\n";
            return $ogTag;

        }
        if(isset($this->_options[0]) && ($this->_options[0] == 'like')){
            if(isset($this->_options[1]) && ($this->_options[0] == 'send')){
                $this->_view->sendButton = '1';
            }
            $this->_view->likeButton = '1';
            $this->_view->fbLanguage = $this->_sessionHelper->fbauthData['fbLanguage'];
            return $this->_view->render('fbcomments.phtml');
        }
        if(isset($this->_options[0]) && ($this->_options[0] == 'comment')){
            $this->_view->AdminLogged = Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PLUGINS);
            $this->_view->pageUrl     = $this->_seotoasterData['websiteUrl'].$this->_seotoasterData['url'];
            $this->_view->commentFb   = '1';
            $limit = '3';
            if(isset($this->_options[1])){
                $this->_view->fbquantity = $this->_options[1];
                $limit                   = $this->_options[1];
            }
            if(!isset($this->_options[2])){
                $this->_view->fbwidth = '300';
            }
            if(isset($this->_options[2])){
                $this->_view->fbwidth = $this->_options[2];
            }
            $url = $this->_seotoasterData['websiteUrl'].$this->_seotoasterData['url'];

            $this->_view->url = $url;
            $queries = array(
                'query1' => 'select post_fbid, fromid, object_id, text, time from comment where object_id in (select comments_fbid from link_stat where url ="'.$url.'") limit '.$limit.'',
                'query2' => 'select post_fbid, fromid, object_id, text, time from comment where object_id in (select post_fbid from #query1)',
                'query3' => 'select name, id, url, pic_square from profile where id in (select fromid from #query1) or id in (select fromid from #query2)',
            );
            $param  =   array(
                'method'    => 'fql.multiquery',
                'queries'   => $queries,
                'callback'  => ''
            );
            $result = $this->_facebook->api($param);
            if(isset($result->error_code) && $result->error_code == '1'){
                $this->_view->errorFb = '1';
                return $this->_view->render('fbcomments.phtml');
            }
            $comments                 = $result[0]['fql_result_set'];
            $this->_view->comments    = $comments;
            $replies                  = $result[1]['fql_result_set'];
            $profiles                 = $result[2]['fql_result_set'];
            $profiles_by_id           = array();
            foreach ($profiles as $profile) {
                $profiles_by_id[$profile['id']] = $profile;
            }
            $this->_view->profile = $profiles_by_id;
            $replies_by_target = array();
            foreach ($replies as $reply) {
                $replies_by_target[$reply['object_id']][] = $reply;
            }
            $this->_view->repliesByTarget = $replies_by_target;
            $this->_view->errorFb = '0';
            $this->_view->fbLanguage = $this->_sessionHelper->fbauthData['fbLanguage'];
            return $this->_view->render('fbcomments.phtml');
        }
        /***********************
         **** Fbcoments end ****
         ***********************/
        if(isset($this->_options[0]) && $this->_options[0] == 'auth'){
            $this->_view->appId		  = $this->_sessionHelper->fbauthData['app_id'];
            $this->_view->callbackUrl = $this->_sessionHelper->fbauthData['redirectUri'];
            $this->_view->currentUser = $this->_sessionHelper->getCurrentUser()->getId();
            return $this->_view->render('fbauth.phtml');
        }
        if(isset($this->_options[0]) && $this->_options[0] == 'album'){
            isset($this->_options[2]) ? $albumName = $this->_options[2] : $albumName = 'SEOTOASTER WORK';
            isset($this->_options[3]) ? $limit = $this->_options[3] : $limit = 20;
            if(isset($this->_options[1])){
                $queries = 'SELECT src_small, src_big, caption FROM photo WHERE aid IN (SELECT aid FROM album WHERE owner IN(\''.$this->_options['1'].'\') AND name IN (\''.($albumName).'\')) ORDER BY created DESC limit '.$limit;
                $param = array(
                    'method' => 'fql.query',
                    'query' => $queries,
                    'callback' => ''
                );
            }
            $this->_view->timestamp = uniqid(time());
            $this->_view->latestImages = $this->_facebook->api($param);
            isset($this->_options[4]) ? $this->_view->size = $this->_options[4] : $this->_view->size = 50;
            return $this->_view->render('album.phtml');
        }
        if(isset($this->_options[0]) && $this->_options[0] == 'albums'){
            isset($this->_options[1]) ? $accountId = $this->_options[1] : $accountId = '305239023366';
            $this->_view->pageUri = $this->_seotoasterData['url'];
            $this->_view->timestamp = uniqid(time());
            isset($this->_options[2]) ? $this->_view->size = $this->_options[2] : $this->_view->size = 50;
            if(isset($this->_sessionHelper->latestImages)){
                $this->_view->goToAlbum = true;
                $this->_view->latestImages = $this->_sessionHelper->latestImages;
                return $this->_view->render('album.phtml');
            }
            $this->_view->albums = $this->_facebookGetUserAlbums($accountId);
            return $this->_view->render('albums.phtml');
        }
		if(isset($this->_options[0]) && $this->_options[0] == 'addUser'){
			$this->_view->accountName = $this->_sessionHelper->fbauthData['user_id'];
			return $this->_view->render('addUser.phtml');
		}
        if(isset($this->_options[0]) && $this->_options[0] == 'photoSearch'){
            isset($this->_options[2]) ? $limit = $this->_options[2] : $limit = 20;
            $this->_view->timestamp = uniqid(time());
            if(isset($this->_options[1])){
                $queries = 'SELECT pid, src, src_small, src_big, caption FROM photo WHERE aid IN (SELECT aid FROM album WHERE owner IN('.$this->_options['1'].')) ORDER BY created DESC limit '.$limit;
                $param = array(
                    'method' => 'fql.query',
                    'query' => $queries,
                    'callback' => ''
                );
            }
            $this->_view->latestImages = $this->_facebook->api($param);
            isset($this->_options['3']) ? $this->_view->size = $this->_options['3'] : $this->_view->size = 50;
            return $this->_view->render('album.phtml');
        }
        if(isset($this->_options[0]) && $this->_options[0] == 'events'){
            $events = $this->_facebook->api('/'.$this->_sessionHelper->fbauthData['user_id'].'/events/?access_token='.$this->_sessionHelper->fbauthData['token']);
            if(isset($events)){
                $this->_view->lastEvent = $events['data'];
            }
            return $this->_view->render('events.phtml');
        }
		if(isset($this->_options[0]) && $this->_options[0] == 'likeBefore'){
			$this->_view->accountName = $this->_sessionHelper->fbauthData['user_id'];
			return $this->_view->render('likeBeforeDownload.phtml');
		}
		$this->_view->currentUser = $this->_sessionHelper->getCurrentUser()->getId();
	    $this->_sessionHelper->pluginPageUrl = $this->_websiteUrl . '.html'; 
	    $this->_view->websiteUrl = $this->_websiteUrl;

        if(isset($this->_options[0]) && $this->_options[0] == 'feed'){
            if(isset($this->_options['1'])) {
                $type = 'type='.$this->_options['1'].'&';
            } else {
                $type = '';
            }
            $statuses = $this->_facebook->api('/'.$this->_sessionHelper->fbauthData['user_id'].'/feed/?'.$type.'access_token='.$this->_sessionHelper->fbauthData['token']);
            isset($this->_options['2']) ? $length = (int) $this->_options['2'] : $length = 1;
            $this->_options['1'] == 'photo' ? $photoClass = ' img_gallery' : $photoClass = '';
            $html = '<ul class="fbgraph_feed'.$photoClass.'">';
            $counter = 0;
            foreach($statuses['data'] as $status){
                if($status['type'] == $this->_options['1']){
                    $html .= $this->_processType(($status));
                    $counter++;
                    if($counter === $length) break;
                }
            }
            $html .= '</ul>';
            $this->_view->status = $html;
            return $this->_view->render('fbgraph.phtml');
        }
        $statuses = $this->_facebook->api('/'.$this->_sessionHelper->fbauthData['user_id'].'/statuses/?access_token='.$this->_sessionHelper->fbauthData['token']);
		$this->_view->status  = $this->_extractStatus($statuses['data'][0]);
		$this->_view->statuses = $statuses;
		return $this->_view->render('fbgraph.phtml');
}
	public function registerAction(){
			$params = array(
			  'scope' => 'read_stream,user_groups,user_events,offline_access, email, manage_pages,publish_stream',
			  'redirect_uri' => $this->_sessionHelper->fbauthData['redirectUri']
		   );
		   $url = $this->_facebook->getLoginUrl($params);
		   header("Location: {$url}");
	}
	public function addTokenAction(){
		if($this->_request->isPost()){
			if(isset ($this->_requestedParams['token'])){
				Models_Mapper_Fbgraph::getInstance()->setToken($this->_requestedParams['token']);
			}
		}
	}
	public function configAction(){
		 $data = Models_Mapper_Fbgraph::getInstance()->getConf();
         $this->_view->translator = $this->_translator;
         $this->_view->fbLanguage = $this->_sessionHelper->fbauthData['fbLanguage'];
		 $data['uri'] = $this->_sessionHelper->fbauthData['redirectUri'];
		 if(!isset($data['app_id'])){
			 $data['app_id'] = 'no application id';
		 } 
		 if(!isset($data['app_secret'])){
			 $data['app_secret'] = 'no application secret';
		 } 
		 if(!isset($data['user_id'])){
			 $data['user_id'] = 'no user';
		 } 
		 $this->_view->appData = $data;
		 $this->_view->websiteUrl = $this->_seotoasterData['websiteUrl'];
		 echo $this->_view->render('config.phtml');
	}
	public function saveConfigAction(){
        if(Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PLUGINS)){
            if($this->_request->isPost()){
                Models_Mapper_Fbgraph::getInstance()->setConf($this->_requestedParams['app_id'],$this->_requestedParams['app_secret'], $this->_requestedParams['userId'],$this->_requestedParams['selectFbLanguage'], $this->_requestedParams['uri']);
            }
		}
		echo '<script>top.location.reload()</script>';
	}

    private function _extractStatus($statusData){
        isset($statusData['message']) && $statusData['message'] != '' ? $status = $statusData['message'] : $status = $statusData['story'];
        return '<a target="_blank" href="http://www.facebook.com/seotoaster/statuses">'.$status.'</a>';
    }
	private function _processType($post){
		switch($post['type']) {
			/*Added new video*/
			case 'video':
                if(isset($post['object_id'])){
                    $query = 'select src_hq from video where vid=\''.$post['object_id'].'\'';
                    $param  =   array(
                        'method'    => 'fql.query',
                        'query'   => $query,
                        'callback'  => ''
                    );
                    $result = $this->_facebook->api($param);
                    $videoSrc = $result[0]['src_hq'];
                } else {
                    $videoSrc = $post['source'];
                }

                isset($this->_options[3]) ? $width = $this->_options[3] : $width  = 320;
                isset($this->_options[4]) ? $height = $this->_options[4] : $height = 240;
				return '<li>
				            <a href="'.$post['link'].'">
							<video width="'.$width.'" height="'.$height.'" controls="controls" title="'.$post['caption'].'">
							      <source src="'.$videoSrc.'" type="video/webm" />
                                  <source src="'.$videoSrc.'" type="video/mp4" />
                                  Your browser does not support the video tag.
                            </video>
                           </li>';
        //	<span>'.$post['message'].'</span>'.$post['name'].'<br>'.$post['caption'].'<br>'.$post['description'].'</p></a>


                break;
			case 'photo':
                isset($this->_options[3]) ? $width = $this->_options[3] : $width  = 50;
                isset($this->_options[4]) ? $height = $this->_options[4] : $height = 50;
				return '<li class="gallery_image">
                            <a class="gall" href="'.$post['picture'].'" rel="gall">
                                <img width="'.$width.'" height="'.$height.'"  src="'.$post['picture'].'" alt="'.$post['story'].'">
                            </a>
    						</li>';//<a href="'.$post['link'].'">'.$post['story'].'</a><image src="'.$post['picture'].'" >
			break;
			/*Uploaded new photo or changed avatar*/
			case 'link':
				isset($post['story']) ? $linkName = $post['story'] : $linkName = $post['description'];
				return '<li><a href="'.$post['link'].'">
							<p><br>'.$linkName.'</p>
						</li></a>';
			break;
			/*Changed text status*/
			case 'status':
				return '<li><p>'.$post['story'].'</p>
						</li>';
			break;
			/*Added new friend*/
			case 'user':
				
			break;
		}
	}
    private function _facebookGetUserAlbums($accountId){
        $fql    = array(
            "query1" => "SELECT aid, cover_pid, name FROM album WHERE owner =(".$accountId.")",
            "query2" => "SELECT src from photo where pid IN (SELECT cover_pid from #query1)"
            );
        $param  =   array(
            'method'    => 'fql.multiquery',
            'queries'     => $fql,
            'callback'  => ''
        );
        return $this->_facebook->api($param);
    }
    private function _facebookGetAlbumImages($albumId){
        $fql    =   "SELECT src_small, src_big, caption FROM photo WHERE aid = '" . $albumId ."'  ORDER BY created DESC";
        $param  =   array(
            'method'    => 'fql.query',
            'query'     => $fql,
            'callback'  => ''
        );
        return $this->_facebook->api($param);
    }

    public function showAlbumAction(){
        $this->_sessionHelper->latestImages = $this->_facebookGetAlbumImages($this->_request->getParam('aid'));
        $this->_redirector->gotoUrl($this->_seotoasterData['websiteUrl'].$this->_request->getParam('redirectUri'), array('exit' => true));
    }
    public function backToAlbumsAction(){
        unset($this->_sessionHelper->latestImages);
        $this->_redirector->gotoUrl($this->_seotoasterData['websiteUrl'].$this->_request->getParam('redirectUri'), array('exit' => true));
    }
    public function loginAction(){
        if($this->_request->isPost() && isset($this->_requestedParams['fullname'])){
            $name  = $this->_requestedParams['fullname'];
            $email = $this->_requestedParams['email'];
            $fbId  = $this->_requestedParams['fbid'];
        } else {
            if (isset($_REQUEST['signed_request'])) {
                $response = $this->parse_signed_request($_REQUEST['signed_request'], $this->_sessionHelper->fbauthData['app_secret']);
                $userData = $response['registration'];
                $name  = $userData['name'];
                $email = $userData['email'];
                $this->sendMail($name, $email);
            }
        }
        if((isset($email) || isset($fbId))  && isset($name)){
            if(!isset($email)) $email = $fbId;
            $user		   =  new Application_Model_Models_User();
            $mapper        =  Application_Model_Mappers_UserMapper::getInstance();
            $users		   = $mapper->fetchAll();
            foreach($users as $key => $value){
                $tableEmail = $value->getEmail();
                $tableRole  = $value->getRoleId();
                if($tableEmail == $email && $tableRole == 'member'){
                    $myKey = $key;
                    $member = $value;
                }
            }
            if(!isset($myKey)){
                $user->setRoleId('member')
                    ->setEmail($email)
                    ->setFullName($name)
                    ->setPassword($this->_sessionHelper->fbauthData['app_secret'].$email);
                $content	   =  $mapper->save($user);
            }
            else {
                $member->setPassword($this->_sessionHelper->fbauthData['app_secret'].$email);
                Application_Model_Mappers_UserMapper::getInstance()->save($member);
            }

            $authAdapter = new Zend_Auth_Adapter_DbTable(
                Zend_Registry::get('dbAdapter'),
                'user',
                'email',
                'password',
                'MD5(?)'
            );
            $authAdapter->setIdentity($email);
            $authAdapter->setCredential($this->_sessionHelper->fbauthData['app_secret'].$email);
            $authResult = $authAdapter->authenticate();
            if($authResult->isValid()) {
                $authUserData = $authAdapter->getResultRowObject(null, 'password');
                if(null !== $authUserData) {
                    $user = new Application_Model_Models_User();
                    $user->setId($authUserData->id);
                    $user->setEmail($authUserData->email);
                    $user->setRoleId($authUserData->role_id);
                    $user->setFullName($authUserData->full_name);
                    $user->setLastLogin(date('Y-m-d H:i:s', time()));
                    $user->setRegDate($authUserData->reg_date);
                    $user->setIpaddress($_SERVER['REMOTE_ADDR']);
                    $this->_sessionHelper->setCurrentUser($user);
                    Application_Model_Mappers_UserMapper::getInstance()->save($user);
                    $this->_cache = Zend_Controller_Action_HelperBroker::getStaticHelper('Cache');
                    $this->_cache->clean('admin_addmenu');
                    unset($user);
                }
            }
            $this->_redirector->gotoUrlAndExit($this->_seotoasterData['websiteUrl'], array('exit' => true));
        }
        else {
            $this->_redirector->gotoUrlAndExit($this->_seotoasterData['websiteUrl'], array('exit' => true));
        }
    }
}
