Fbgraph allows you to use all abilities of Facebook Graph API and connect your Facebook account or fanpage to your Seotoaster 2 account.
To use this plugin you need enter application id,secret key of your application and Facebook user account id.
Remember that plugin can get only public data from your Facebook pag.
To start use plugin:
    Create page facebookpermissions and add on page shortcut {$plugin:fbgraph:addUser}. Allow all permissions to your Facebook app and you cat start use plugin.
--Events--
Add {$plugin:fbgraph:events} shortcut on page and plugin bring all your Facebook account events starting soon.

--Albums--
Add  {$plugin:fbgraph:albums:user_account_id:thumbnails_size} and you get all your public albums.

{$plugin:fbgraph:album:user_account_id:album_name:images_limit:icons_size} bring to site only album_name album
icon_size - size of images (height and width). Default icon_size value is 50 px

You also can get latest content of different types from your wall:
    - for video:
        type {$plugin:fbgraph:feed:video:number_of_videos:width:height}
    - for photo:
            {$plugin:fbgraph:feed:photo:number_of_photos:width:height}
    - for statuses:
            {$plugin:fbgraph:feed:photo:number_of_statuses}
    - for links:
             {$plugin:fbgraph:feed:photo:number_of_links}
-----------------Meta tags---------------------------------
This options add "facebook Open Graph Protocol" meta tags to pages of your website.
When someone browse your website, this person can share it with their friends in "facebook".

After click on "Share", user get "facebook" pop-up window and confirm share there.
Then this user get to his "facebook" page a description of your website page (H1 tag, teaser image, teaser text and name of website).

How to use it:
Plugin have 4 mods:
- tags
- share
- ogurl
- ogtag

On admin panel choose a "Layout" then "Edit this template" and put there {$plugin:fbgraph:tags} between <head> </head> tags.

And you can add a "Share" button for page.
{$plugin:fbgraph:share}

And also you can get just url of current page.
{$plugin:fbgraph:ogurl}

-----------Comments-------------------------
This options is necessary for indexation the page with robots on which the Facebook a system of comments situated.

How to use it:
First after installation click on admin panel Fbgraph. Insert your admin id. And choose language.
Then click edit template and insert near meta in the head section {$plugin:fbgraph:ogtag}
After click on "container" (C) where do you want to place your Facebook system of comments or "like" button.
Then enter {$plugin:fbgraph:comment:20:200}. The parameter '20' you can change for your purposes.
It is a quantity of hiding and not hiding tags with information from your comment system such as: text, name of user, links which necessary for indexation the page with robots.
The parameter '200' you can change for your purposes. It is a width of Facebook plugin.
Then click to fbgraph in admin panel. Enter your account key from facebook and then save.
Or {$plugin:fbgraph:like} for adding like button.
Important!!!
Plugin does not work if you not establish parameter {$plugin:fbgraph:comment:20:200}. Always establish quantity parameter.

Important!!! You must have fb:admin meta tag. For this you need add into the "tag" "head" in template {$plugin:fbgraph:ogtag}

---------Photo search-----------------
{$plugin:fbgraph:photoSearch:facebook_accounts_id:number_of_images:thumbnails_size}

    "facebook_accounts_id" - insert instead facebook_accounts_id facebook user or users account ID's. If you want search into few accounts albums,
        insert IDs divided by ',' (for example id_1,id_2,id_3) .
    "number_of_images" - amount of latest photos you want to get
    "thumbnails_size" - size (width and height) of image's previews.